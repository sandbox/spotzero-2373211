<?php

/**
 * Implements hook_ctools_plugin_directory().
 */
function node_comments_language_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Edit of comment_get_thread to filter by the current site langauge.
 */
function node_comments_language_comment_get_thread($node, $mode, $comments_per_page) {
  global $language;

  $query = db_select('comment', 'c')->extend('PagerDefault');
  $query->addField('c', 'cid');
  $query->condition('c.language', $language->language);
  $query->condition('c.nid', $node->nid)->addTag('node_access')->addTag('comment_filter')->addMetaData('node', $node)->limit($comments_per_page);

  $count_query = db_select('comment', 'c');
  $count_query->addExpression('COUNT(*)');
  $count_query->condition('c.language', $language->language);
  $count_query->condition('c.nid', $node->nid)->addTag('node_access')->addTag('comment_filter')->addMetaData('node', $node);

  if (!user_access('administer comments')) {
    $query->condition('c.status', COMMENT_PUBLISHED);
    $count_query->condition('c.status', COMMENT_PUBLISHED);
  }
  if ($mode === COMMENT_MODE_FLAT) {
    $query->orderBy('c.cid', 'ASC');
  }
  else {
    // See comment above. Analysis reveals that this doesn't cost too
    // much. It scales much much better than having the whole comment
    // structure.
    $query->addExpression('SUBSTRING(c.thread, 1, (LENGTH(c.thread) - 1))', 'torder');
    $query->orderBy('torder', 'ASC');
  }

  $query->setCountQuery($count_query);
  $cids = $query->execute()->fetchCol();

  return $cids;
}
